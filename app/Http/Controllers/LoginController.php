<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    public function login()
    {
        return view('login.login'); 
    }

    public function login_store(Request $request)
    {
        if(User::loginUser($request->email,$request->password))//Check UserName and Password
        {
            $role = User::where('email','=',$request->email)->pluck('role')->first();
            $verify = User::where('email','=',$request->email)->pluck('verified')->first();

            if($role=='superadmin'){
                return \Redirect::route('admin.view');
            }            
            elseif($role=='user' && $verify=="1"){                
                return \Redirect::route('user.view');
            }            
            else
            {
                // notify()->error('Invalid Email Or Password !');
                return redirect()->back();
            }
        }
        else
        {
            // notify()->error('Invalid Email Or Password !');
            return redirect()->back();
        }
    }

    public function logout()
    {
       \Auth::logout();
        return \Redirect::route('task_login');
    }
}
