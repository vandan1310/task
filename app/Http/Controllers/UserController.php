<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function view()
    {
        return view('user.pages.view');
    }

    public function profile_update(Request $request)
    {
        $files = Input::file('profile_pic');
        $picName = str_random(30).'.'.$files->getClientOriginalExtension(); 
        $files->move(public_path().'/'.'Profile/',$picName); 
        
        \DB::table('users')
            ->where('id',\Auth::user()->id)
            ->update([
                'first_name'=>$request->get('first_name'),
                'last_name'=>$request->get('last_name'),
                'contact'=>$request->get('contact'),
                'email'=>$request->get('email'),
                'profile_pic'=>$picName
            ]);
            return back();
    }
}
