<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function view()
    {
        $user = User::where('role','=','user')->get();
        return view('admin.pages.view',compact('user'));
    }

    public function read($id)
    {
        \DB::table('users')
            ->where('id',$id)
            ->update([
                'permission'=>'read'
            ]);
            return back();
    }

    public function write($id)
    {
        \DB::table('users')
            ->where('id',$id)
            ->update([
                'permission'=>'write'
            ]);
            return back();
    }

    public function remove($id)
    {
        $user=User::find($id);
        $user->delete();
        return back();
    }
}
