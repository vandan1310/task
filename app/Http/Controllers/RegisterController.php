<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Notification;
use App\VerifyUser;
use App\Mail\VerifyMail;
use Mail;
use App\Notifications\EmailNotification;
use Illuminate\Auth\Access\Response;

class RegisterController extends Controller
{
    public function register()
    {
        return view('login.register');
    }

    public function ajaxRequestPost(Request $request)
    {
        $users = \DB::table('users')->count();
        if($users){

            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'contact' => 'required',
                'email' => 'required|email',
                'password' => 'required|confirmed',
                'profile_pic' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
              ]);
    
              if ($validator->passes()) {    
                $input = $request->all();
                $input['role'] = 'user'; 
                $input['password'] = bcrypt($input['password']);
                $extension = $request->file('profile_pic')->getClientOriginalExtension();
                $dir = 'Profile/';
                $input['profile_pic'] = uniqid() . '_' . time() . '.' . $extension;
                $request->file('profile_pic')->move($dir, $input['profile_pic']);                
                User::create($input);            
                $data = User::where('email',$request->get('email'))->first();
                VerifyUser::create([
                  'user_id' => $data->id,
                  'token' => sha1(time())
                ]);
                \Mail::to($data->email)->send(new VerifyMail($data));
                return response()->json();
                }
              return response()->json(['error'=>$validator->errors()->all()]);
        }
        elseif($users=="null"){
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'contact' => 'required',
                'email' => 'required|email',
                'password' => 'required|confirmed',
                'profile_pic' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
              ]);
    
              if ($validator->passes()) {    
                $input = $request->all();
                $input['role'] = 'superadmin';
                $input['password'] = bcrypt($input['password']);                   
                $extension = $request->file('profile_pic')->getClientOriginalExtension();
                $dir = 'Profile/';
                $input['profile_pic'] = uniqid() . '_' . time() . '.' . $extension;
                $request->file('profile_pic')->move($dir, $input['profile_pic']);                
                User::create($input);            
                $data = User::where('email',$request->get('email'))->first();
                // $data->notify(new EmailNotification);
                VerifyUser::create([
                  'user_id' => $data->id,
                  'token' => sha1(time())
                ]);
                \Mail::to($data->email)->send(new VerifyMail($data));
                return response()->json();
                }
              return response()->json(['error'=>$validator->errors()->all()]);
        }        
    }
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return \Redirect::route('task_login')->with('warning', "Sorry your email cannot be identified.");
        }
        return \Redirect::route('task_login')->with('status', $status);
    }
}
