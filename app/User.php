<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name','email', 'contact','password','profile_pic','role','permission','verified'
    ];

    public function verifyUser()
    {
         return $this->hasOne('App\VerifyUser');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return($this->role=='superadmin');
    }

    public function isUser()
    {
        return ($this->role=="user");
    }

    public static function loginUser($email,$password)
    {
        return \Auth::attempt(array('email' => $email, 'password' => $password));
    }
}
