<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('logout','LoginController@logout');
Route::get('/',array('as'=>'task_login','uses'=>'LoginController@login'));
Route::post('login/store',array('as'=>'login.store','uses'=>'LoginController@login_store'));
Route::get('signup',array('as'=>'task_register','uses'=>'RegisterController@register'));
Route::post('register/store', 'RegisterController@ajaxRequestPost')->name('register_store');
Route::get('/user/verify/{token}', 'RegisterController@verifyUser');

Route::group(['middleware' => 'superadmin'], function(){
    Route::get('admin/view','AdminController@view')->name('admin.view');
    Route::get('permission/read/{id}','AdminController@read')->name('read');
    Route::get('permission/write/{id}','AdminController@write')->name('write');
    Route::get('user/remove/{id}','AdminController@remove')->name('remove');
});

Route::group(['middleware' => 'user'], function(){
    Route::get('user/view',array('as'=>'user.view','uses'=>'UserController@view'));
    Route::post('user/profilestore',array('as'=>'profile_update','uses'=>'UserController@profile_update'));
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
