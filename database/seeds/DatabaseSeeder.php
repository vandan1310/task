<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        // $this->call(UsersTableSeeder::class);

        $users = app('db')->table('users');
        $password = bcrypt('1234');
        $position = 0;

        for($i = 0; $i < 100; $i++){
            $list = [];
            for($j = 0; $j <= 1000; $j++){
                $list[] = [
                    'role'=>'user',
                    'first_name' => 'First Name '.$position,
                    'last_name' => 'Last Name'.$position,
                    'contact' => '1234567890',
                    'email' => "example.{$position}@example.com",
                    'password' => $password,
                    'profile_pic' => '-',
                ]; 
                $position++;
            }
            $users->insert($list);
        }
    }
}
