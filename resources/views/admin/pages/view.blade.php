@extends('admin.layouts.app')
@section('content')
<section class="content-header">
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">User List</h3>
              </div>
              <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Email</th>
                      <th>Contact</th>
                      <td>Permission</td>                      
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($user as $list)
                    <tr>
                      <td>{{$list->first_name}}</td>
                      <td>{{$list->last_name}}</td>
                      <td>{{$list->email}}</td>
                      <td>{{$list->contact}}</td>
                      <td>                      
                        <a href="{{route('read',$list->id)}}" class="btn btn-success">Read</a>                    
                        <a href="{{route('write',$list->id)}}" class="btn btn-primary">Write</a>
                      </td>                      
                      <td>
                        <a href="{{route('remove',$list->id)}}" class="btn btn-danger">Delete</a>
                      </td>
                    </tr>
                    @endforeach 
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
</section>
@endsection
