<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
        </div>
        <!-- search form
       <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
         /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                  <i class="fa fa-files-o"></i>
                  <span>Category Panel</span>
                </a>
                <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-circle-o"></i> Category Add/View</a></li> 
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                  <i class="fa fa-files-o"></i>
                  <span>Product Panel</span>
                </a>
                <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-circle-o"></i> Product Add</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Product View</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>