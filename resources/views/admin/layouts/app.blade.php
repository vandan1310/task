<!DOCTYPE html>
<html>
<head>
    @include('admin.layouts.partials.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('admin.layouts.partials.header')
    @include('admin.layouts.partials.sidebar')

    <div class="content-wrapper">
        @yield('content')
    </div>
    @include('admin.layouts.partials.footer')
</div>
@include('admin.layouts.partials.js')
</body>
</html>
