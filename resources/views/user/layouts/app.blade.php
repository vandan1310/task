<!DOCTYPE html>
<html>
<head>
    @include('user.layouts.partials.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('user.layouts.partials.header')
    @include('user.layouts.partials.sidebar')

    <div class="content-wrapper">
        @yield('content')
    </div>
    @include('user.layouts.partials.footer')
</div>
@include('user.layouts.partials.js')
</body>
</html>
