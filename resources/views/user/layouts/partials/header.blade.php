<header class="main-header">
    <a href="#" class="logo">
        <span class="logo-lg">{{\Auth::user()->first_name}}</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <table align="right">
            <tr>
                <td><a href="{{route('logout')}}" class="btn btn-primary btn-lg"> Log-out </a> </td></tr>
        </table>
    </nav>
</header>