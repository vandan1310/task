@extends('user.layouts.app')
@section('content')
<section class="content">
  @if(\Auth::user()->permission == "read" || \Auth::user()->permission == "")
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">{{\Auth::user()->first_name}} Profile</h3>
            <span style="color:crimson">You Have Only Read Access</span>
        </div>
        <form role="form">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">First_name</label>
              <input type="email" name="first_name" disabled class="form-control" value="{{\Auth::user()->first_name}}" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Last_name</label>
              <input type="email" name="last_name" disabled class="form-control" value="{{\Auth::user()->last_name}}" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" name="email" disabled class="form-control" value="{{\Auth::user()->email}}" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Contact</label>
              <input type="text" name="last_name" class="form-control" value="{{\Auth::user()->contact}}" placeholder="Enter Contact" disabled>
            </div>
            <div class="form-group">
              <div class="avatar-upload">
                  <div class="avatar-edit">
                      <input type='file' name="profile_pic" disabled id="imageUpload" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload"></label>
                  </div>
                  <div class="avatar-preview">
                      <div id="imagePreview" style="background-image: url({{asset('Profile/'.\Auth::user()->profile_pic)}});">
                      </div>
                  </div>
              </div>
          </div> 
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div> 
  @elseif(\Auth::user()->permission == "write")
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">{{\Auth::user()->first_name}} Profile</h3>
          <span style="color:green"> You Have Write Access</span>
        </div>
        <form role="form" action="{{route('profile_update')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">First_name</label>
              <input type="text" name="first_name" class="form-control" value="{{\Auth::user()->first_name}}" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Last_name</label>
              <input type="text" name="last_name" class="form-control" value="{{\Auth::user()->last_name}}" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Contact</label>
              <input type="number" name="contact" class="form-control" value="{{\Auth::user()->contact}}" placeholder="Enter Contact">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" name="email" class="form-control" value="{{\Auth::user()->email}}" placeholder="Enter email">
            </div>
            <div class="form-group">
              <div class="avatar-upload">
                  <div class="avatar-edit">
                      <input type='file' name="profile_pic" id="imageUpload" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload"></label>
                  </div>
                  <div class="avatar-preview">
                      <div id="imagePreview" style="background-image: url({{asset('Profile/'.\Auth::user()->profile_pic)}});">
                      </div>
                  </div>
              </div>
          </div> 
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  @endif
</section>
@endsection
